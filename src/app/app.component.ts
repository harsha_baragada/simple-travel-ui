import { AuthService } from './common/services/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  auth = localStorage.getItem('_auth');
  expireDate = localStorage.getItem('expireDate');
  constructor(private readonly authService: AuthService) {
    this.getAuthenticaton();
  }
  getAuthenticaton() {
    let now = new Date().getTime();
    this.authService.obtainAccessToken().subscribe((tokenResponse) => {
      localStorage.setItem('_auth', tokenResponse.access_token);
      localStorage.setItem(
        'expiry',
        (now + tokenResponse.expires_in).toString()
      );
    });
  }
}
