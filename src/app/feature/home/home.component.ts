import { SearchParams } from './../../common/model/search-params.model';
import { FareService } from './../../common/services/fare.service';
import { QueryParams } from './../../common/model/query.model';
import { AirportService } from './../../common/services/airport.service';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Observable } from 'rxjs';
import {
  startWith,
  map,
  debounceTime,
  tap,
  switchMap,
  finalize,
} from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  flightSearchForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  airports: any[] = [];
  fare: any;
  isLoading: boolean;
  queryParams: QueryParams = {
    originCode: '',
    destinationCode: '',
    currency: '',
  };
  searchParams: SearchParams;

  originAiports: any[];
  destinationAirports: any[];

  search(event) {
    console.log(event);
  }

  constructor(
    private readonly airportService: AirportService,
    private _snackBar: MatSnackBar,
    private readonly fareService: FareService
  ) {
    this.flightSearchForm = this.createFlightSearchForm();
  }

  ngOnInit(): void {
    this.flightSearchForm
      .get('originFormControl')
      .valueChanges.pipe(
        debounceTime(100),
        tap(() => (this.isLoading = true)),
        switchMap((value) =>
          this.airportService
            .getAirports(value)
            .pipe(finalize(() => (this.isLoading = false)))
        )
      )
      .subscribe(
        (airports) => (this.originAiports = airports._embedded.locations)
      );

    this.flightSearchForm
      .get('destinationFormControl')
      .valueChanges.pipe(
        debounceTime(100),
        tap(() => (this.isLoading = true)),
        switchMap((value) =>
          this.airportService
            .getAirports(value)
            .pipe(finalize(() => (this.isLoading = false)))
        )
      )
      .subscribe(
        (airports) => (this.destinationAirports = airports._embedded.locations)
      );
  }

  createFlightSearchForm() {
    return new FormGroup({
      originFormControl: new FormControl('', [Validators.required]),
      destinationFormControl: new FormControl('', Validators.required),
      currencyFormControl: new FormControl('EUR', Validators.required),
    });
  }

  getFare() {
    if (this.flightSearchForm.valid) {
      this.isLoading = true;
      let origin = this.flightSearchForm.controls.originFormControl.value.code;
      let destination = this.flightSearchForm.controls.destinationFormControl
        .value.code;
      this.queryParams.currency = this.flightSearchForm.controls.currencyFormControl.value;
      if (origin === destination) {
        this.openSnackBar('Destination should not be same as origin', 'close');
      } else {
        this.queryParams.originCode = origin;
        this.queryParams.destinationCode = destination;

        this.fareService.getFareOffer(this.queryParams).subscribe((fare) => {
          console.log(fare);
          this.fare = fare;
          this.isLoading = false;
        });
      }
    }
  }
  reset() {
    this.flightSearchForm.reset();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  displayFn(airport: any) {
    if (airport) {
      return airport.description;
    }
  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
