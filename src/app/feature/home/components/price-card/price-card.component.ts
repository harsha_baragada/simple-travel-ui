import { AirportService } from './../../../../common/services/airport.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-price-card',
  templateUrl: './price-card.component.html',
  styleUrls: ['./price-card.component.css'],
})
export class PriceCardComponent implements OnInit {
  @Input() fare: any;
  origin: any = '';
  destination: any = '';
  constructor(private readonly airportService: AirportService) {}

  ngOnInit(): void {
    this.airportService
      .getAirportDetails(this.fare.origin)
      .subscribe((airportDetails) => {
        this.origin = airportDetails;
      });
    this.airportService
      .getAirportDetails(this.fare.destination)
      .subscribe((airportDetails) => {
        this.destination = airportDetails;
      });
  }
}
