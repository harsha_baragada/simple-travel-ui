import { AirportService } from './../../common/services/airport.service';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-airports-info',
  templateUrl: './airports-info.component.html',
  styleUrls: ['./airports-info.component.css'],
})
export class AirportsInfoComponent implements AfterViewInit {
  displayedColumns: string[] = ['code', 'name', 'description', 'location'];
  dataSource: MatTableDataSource<any>;
  aiports: any[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private readonly airportsService: AirportService) {
    this.airportsService.getAllAirports().subscribe((resp) => {
      console.log(resp.body);
      this.aiports = resp._embedded.locations;
      this.dataSource = new MatTableDataSource(this.aiports);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
