export class AppConfig {
  public static restUrl = 'http://localhost:8080';
  public static restEndpoint = {
    airports: '/airports',
  };
}
