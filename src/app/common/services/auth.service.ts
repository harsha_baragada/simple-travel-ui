import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable()
export class AuthService {
  constructor(private _http: HttpClient) {}

  obtainAccessToken(): Observable<any> {
    const body = new HttpParams().set('grant_type', 'client_credentials');

    return this._http.post('http://localhost:8080/oauth/token', body, {
      headers: { skip: 'true' },
    });
  }
}
