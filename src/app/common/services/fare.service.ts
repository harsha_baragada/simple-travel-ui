import { QueryParams } from './../model/query.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class FareService {
  constructor(private readonly http: HttpClient) {}

  getFareOffer(queryParams: QueryParams): Observable<any> {
    return this.http.get(
      'http://localhost:8080/fares/' +
        queryParams.originCode +
        '/' +
        queryParams.destinationCode +
        '?currency=' +
        queryParams.currency
    );
  }
}
/* ,
      {
        headers: new HttpHeaders({
          'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
          Authorization: 'Bearer ' + localStorage.getItem('_auth'),
        }),
      } */
