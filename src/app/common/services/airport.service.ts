import { AppConfig } from './../config/app.config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
@Injectable()
export class AirportService {
  constructor(private _http: HttpClient) {}

  getAirports(params: any): Observable<any> {
    if (typeof params === 'string') {
      return this._http.get(
        AppConfig.restUrl + AppConfig.restEndpoint.airports + '?term=' + params
      );
    }
    return EMPTY;
  }

  getAllAirports(): Observable<any> {
    return this._http.get(
      AppConfig.restUrl + AppConfig.restEndpoint.airports + '?size=' + 1048
    );
  }

  getAirportDetails(airportCode: any): Observable<any> {
    return this._http.get(
      AppConfig.restUrl + AppConfig.restEndpoint.airports + '/' + airportCode
    );
  }
}
