import { AuthService } from './../services/auth.service';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class OauthHttpInterceptor implements HttpInterceptor {
  constructor(private readonly authService: AuthService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const expiry = Number(localStorage.getItem('expiry'));
    const auth = localStorage.getItem('_auth');
    let clone: HttpRequest<any>;
    if (request.headers.get('skip')) {
      clone = request.clone({
        setHeaders: {
          'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
          Authorization: 'Basic ' + btoa('travel-api-client:psw'),
        },
        headers: request.headers.delete('skip'),
      });
    } else {
      let now = new Date().getTime();
      if (expiry < now) {
        // renew token
        this.authService.obtainAccessToken().subscribe((tokenResponse) => {
          localStorage.setItem('_auth', tokenResponse.access_token);
          localStorage.setItem(
            'expiry',
            (new Date().getTime() + 1000 * tokenResponse.expires_in).toString()
          );
        });
      }
      clone = request.clone({
        setHeaders: {
          'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
          Authorization: 'Bearer ' + auth,
        },
      });
    }

    return next.handle(clone);
  }
}
