export interface SearchParams {
  size: number;
  page: number;
  lang: string;
  term: string;
}
