export interface QueryParams {
  originCode: string;
  destinationCode: string;
  currency: string;
}
